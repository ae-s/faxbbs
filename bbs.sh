#!/bin/bash

set +x

expect -f prep.expect

echo -en 'AT%T\r\n' > /dev/ttyUSB0
cat /dev/ttyUSB0 &
pid=$!
sleep 10 && kill $pid
sleep 0.5

exten=$(cat tones)

echo "got some digits: $exten"
echo "\r\n" > /dev/ttyUSB0

expect -f continue.expect -- "$exten"

