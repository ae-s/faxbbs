#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pty.h>
#include <utmp.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>

int fd;

// stolen from http://www.iso-9899.info/wiki/Code_snippets:hexdump
static void hexdump(char* tag, const void * memory, size_t bytes) {
  const unsigned char * p, * q;
  int i;
  
  p = memory;
  while (bytes) {
    q = p;
    printf("%s: ", tag);
    for (i = 0; i < 16 && bytes; ++i) {
      printf("%02X ", *p);
      ++p;
      --bytes;
    }
    bytes += i;
    while (i < 16) {
      printf("XX ");
      ++i;
    }
    printf("| ");
    p = q;
    for (i = 0; i < 16 && bytes; ++i) {
      printf("%c", isprint(*p) && !isspace(*p) ? *p : ' ');
      ++p;
      --bytes;
    }
    while (i < 16) {
      printf(" ");
      ++i;
    }
    printf(" |\n");
  }
  return;
}

// t is in 0.1 second units
int set_timeout(int t)
{
  struct termios termios;
  tcgetattr(fd, &termios);
  termios.c_cc[VTIME] = t;
  termios.c_cc[VMIN] = 0.2;
  tcsetattr(fd, TCSANOW, &termios);
  return 0;
}

void configure(void)
{
  struct termios termios;
  tcgetattr(fd, &termios);
  cfmakeraw(&termios);
  cfsetspeed(&termios, B19200);
  tcsetattr(fd, TCSANOW, &termios);
}

bool get(char* want, char** got, int* ptr, int tries)
{
  ssize_t sz;
  const size_t maxlen = 1024;
  char* ix;
  *got = malloc(maxlen * sizeof(char));
  if (*got == NULL) {
    *ptr = 0;
    return false;
  }

  do {
    sz = read(fd, *got, maxlen);
    (*got)[sz] = '\0';
    ix = strstr(*got, want);
    printf("got %d bytes\n", (int)sz);
    hexdump("rcv", (void*)*got, sz);
  } while ((--tries > 0) && (ix == NULL));

  if (ix != NULL)
    *ptr = ix - *got;

  return ix != NULL;
}

bool expect(char* want, int tries)
{
  char* got;
  int ix;
  bool res = get(want, &got, &ix, tries);
  if (got != NULL) free(got);
  usleep(10000);
  return res;
}

void put(char* text)
{
  size_t len = strlen(text);
  int ix;
  hexdump("snd", text, len);
  for (ix = 0; ix < len; ix++) {
    write(fd, &text[ix], 1);
    usleep(20000);
    putchar(text[ix]);
    fflush(stdout);
  }
}

int main(int argc, char** argv)
{
  fd = open("/dev/ttyUSB0", O_SYNC|O_RDWR);

  configure();
  set_timeout(100); // quit reading after 10 seconds

  sleep(1);
  // reset modem
  put("ATZ\r\n");
  expect("OK", 2);

  // i don't need to be echoed, thank you very much.
  //put("ATE0\r\n");
  //expect("OK", 4);

  // go into fancyfax mode
  put("AT+FCLASS=2.0\r\n");
  expect("OK", 2);

  // don't even try to allow the far end to go into data mode.  fuck
  // completely off with that computer bullshit.
  put("AT+FAA=0\r\n");
  expect("OK", 2);

  // tell me alllll about image exchange.  but not nonstandard
  // negotiation frames, i don't want to hear about any of your weird
  // kinks.  consenting adults and all that.
  put("AT+FNR=1,1,1,0\r\n");
  expect("OK", 2);

  // bit-ordering and eol alignment
  put("AT+FBO=1+FEA=0\r\n");
  expect("OK", 1);

  // hardware handshaking
  put("AT+FLO=2\r\n");
  expect("OK", 2);

  put("AT+FLI=\"Shadytel Fax BBS\"\r\n");
  expect("OK", 2);

  while (true) {
    put("ATH\r\n");
    expect("OK", 2);

    // yes of course we have pixels waiting, if you want them.
    // also we can receive, i guess.
    //
    // unfortunately the calling station (i.e., not the bbs) gets to
    // decide the mode, and we have zero input on it :(
    put("AT+FLP=1+FCR=1\r\n");
    expect("OK", 2);

    // when the phone rings, answer it!! but don't try to be a fax yet
    // runtime is limited to 1 day
    expect("RING", 8640);
    put("ATH1\r\n");
    expect("OK", 2);
    // some kind of dialprompt: dtmf 44
    put("ATX0DT44;\r\n");
    expect("OK", 3);

    put("AT%T\r");
    {
      int ix; char* dialstr; bool got;
      got = get("#", &dialstr, &ix, 3);
      printf("result is %d, dialled string is [%s] and index is %d\n", got, dialstr, ix);
    }
  }
}
